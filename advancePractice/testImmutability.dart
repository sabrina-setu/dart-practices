import 'package:meta/meta.dart';

@immutable 
class User {
  final String name;
  User(this.name);
  User.withLog(this.name)
  {
    print('User created with name: ${this.name}');
  }
  
}
void main() {
  var u = User.withLog('John Doe');
  print(u.name);
}