enum Colors{
  blue,
  red,
  green,
  white,
  black,
}
 void main(){
   var myColor = Colors.red;

   switch(myColor){
     case Colors.red: {
       print('You are very romantic');
       break;
     }
     case Colors.black:{
       print('You are very fashionable');
       break;
     }
     default: {
       print('You are just ordinary');
     }
   }
   if(myColor==Colors.white){
     print('You are very Pure');
   }

   print('Colors.white.value => ${Colors.white.index}');

   print('Colors.white.values => ${Colors.values}');
 }