import 'package:meta/meta.dart';

getFullName({@required String firstName,@required String lastName}){
  return '$firstName $lastName';
}

class  Person {
  String firstName, lastName;
  Person({@required this.firstName,@required this.lastName});
  String get fullName => getFullName(firstName: firstName, lastName: lastName);
  
}
void main(){
  Person p = Person(firstName: 'John', lastName: 'Doe');
  print('p.fullName => ${p.fullName}');
}
