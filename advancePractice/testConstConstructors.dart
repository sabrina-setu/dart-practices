class RegularPoint {
  final int x;
  final int y;

  RegularPoint(this.x,this.y);
  
}

class PerfectPoint {
  final int x;
  final int y;

  const PerfectPoint(this.x,this.y);
  const PerfectPoint.named({this.x,this.y});
  
}

void main()
{
  var rp1= RegularPoint(1,1);
  var rp2= RegularPoint(1,1);
  print('rp1 == rp2 : ${rp1 == rp2}');

  var pp1 = const PerfectPoint(1, 1);
  var pp2 = const PerfectPoint(1, 1);
  var pp3 = const PerfectPoint.named(x: 1, y:1);
  var pp4 = PerfectPoint(1, 1);
  print('pp1 == pp2 : ${pp1 == pp2}');
  print('pp1 == pp3 : ${pp1 == pp3}');
  print('pp1 == pp4 : ${pp1 == pp4}');
}