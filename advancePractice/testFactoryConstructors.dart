import 'package:uuid/uuid.dart';

var uuidGenerator = Uuid();

class Person {
  int id;
  String name;
  String uuid;

  static final Map<int,Person>_cache={};
  factory Person(int id,String name){
    if(!_cache.containsKey(id))
    {
      print('Creating new instance with id $id');
      var uuid=uuidGenerator.v4();
      var instance= Person._createInstance(id,name,uuid);
      _cache[id]=instance;
    }
    return _cache[id];

  }
  Person._createInstance(this.id,this.name,this.uuid);
  @override
  String toString()
  {
    return 'id: ${this.id}, name: ${this.name}, uuid: ${this.uuid}';
  }
}

void main(){
  Person p1=Person(1,'John');
  print('p1: $p1');

  Person p2= Person(1,'Mike');
  print('p2: $p2');

  Person p3 =Person(2,'Mike');
  print('p3: $p3');
}