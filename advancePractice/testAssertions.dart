class Person extends Object {
  int id;
  String name;
  int age;

  Person({this.id,this.name,this.age}) : assert (id != null) , assert(name != null ,'Arrgument name should not be null.'), super();
  Person.Create({id,name,age}) : super(){
    assert(id != null);
    assert(name != null , 'you must provide namw of the person.');
    this.id=id;
    this.name=name;
    this.age=age;
  }

  @override
  String toString(){
    return 'id : ${this.id} ,name : ${this.name}, age : ${this.age} ';
  }
  
}

void main()
{
  Person p = Person(id: 1);
  print('p: $p');
}