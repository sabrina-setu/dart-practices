void example_one(){
  List<String> list_1 =["Mango","Banana","Apple"];
  List<String> list_2 =["Friends","BBT","Suits"];

  List<String> list_merged_old = [];
  list_merged_old.addAll(list_1);
  list_merged_old.addAll(list_2);

  List<String> list_merged_new=[...list_1,...list_2];
  print("example_one_old :=> $list_merged_old");
  print("example_one_new :=> $list_merged_new");
}
void example_two(){
  List<int> numbers =[1,2,3,4,5,6,7,8];
  
  List<int> numbers_filtered_old = numbers.where((number) => number%2 == 0) . toList();

  List<int> numbers_filtered_new=[...numbers.where((number) => number%2 == 0)];
  print("example_one_old :=> $numbers_filtered_old");
  print("example_one_new :=> $numbers_filtered_new");
}
void main(){
  example_one();
  example_two();
}
