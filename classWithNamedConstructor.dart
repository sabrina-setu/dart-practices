

void main(){
//create 'ross' object
Person ross = Person.initWithUpperCase('Ross','Geller');

print("FullName: ${ross.getFullName()}");
}
//Simple Person Class
class Person{

  //instance variables with 'null' value
  String firsName, lastName;

  //instance variables with initial '18' value
  var age ;

  //default constructor function
  Person(this.firsName,this.lastName,[this.age=18]);
  
  //named constructor: initWithUpperCase
  Person.initWithUpperCase(this.firsName,this.lastName,[this.age=18])
  {
    this.firsName=firsName.toUpperCase();
    this.lastName=lastName.toUpperCase();
    this.age=age;
  }

  

  //instance methods
  String getFullName (){
    return this.firsName+ " "+this.lastName; // 'this' points to the object itself
  }
}