// import 'dart:math'; 
// void main() { 
//    print("Square root of 36 is: ${sqrt(36)}"); 
// }
// library loggerlib;                            
// void _log(msg) {
//    print("Log method called in loggerlib msg:$msg");      
// } 
// import 'test.dart' as web; 
// void main() { 
//    web._log("hello from webloggerlib"); 
// } 
// import 'calculator.dart';  
// void main() {
//    var num1 = 10; 
//    var num2 = 20; 
//    var sum = add(num1,num2); 
//    var mod = modulus(num1,num2); 
//    var r = random(10);  
   
//    print("$num1 + $num2 = $sum"); 
//    print("$num1 % $num2= $mod"); 
//    print("random no $r"); 
// } 
import 'loggerlib.dart'; 
import 'webloggerlib.dart' as web;  

// prefix avoids function name clashes 
void main(){ 
   log("hello from loggerlib"); 
   web.log("hello from webloggerlib"); 
} 