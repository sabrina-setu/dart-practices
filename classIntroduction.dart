

void main(){
//create 'ross' object
Person ross = new Person();
ross.firsName = 'Ross';
ross.lastName='Geller';

print("FullName: ${ross.getFullName()}");
print("ross object : $ross");
}
//Simple Person Class
class Person{
  //A static variables are accessed on the class
  static String company = 'Awesome LLC';

  //instance variables with 'null' value
  String firsName, lastName;

  //instance variables with initial '18' value
  var age= 18 ;

  //instance methods
  String getFullName (){
    return this.firsName+ " "+this.lastName; // 'this' points to the object itself
  }

  //static method
  static String info(){
    return 'This is a Person Class';
  }

  //toString instance method to print string represantation of the object
  //it overrides 'tostring' method defined in 'Object' Class
  @override
  String toString(){
    //since 'company' is a static variable, it should be accessed on 'person' class
    return "Name: ${this.getFullName()}, Age: ${this.age},Company: ${Person.company},Info:${Person.info()}";
  }
}