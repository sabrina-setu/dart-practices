
void main(){

//create an Employee object
var e =Employee('Ross', 'Geller',1000);
print('Employee e : ${e.firstName} ${e.lastName}, salary : ${e.salary}');

//create Student object
var s =Student('Joey', 'Green',418);
print('Student s : ${s.firstName} ${s.lastName}, score : ${s.score}');

//create Person object
var p =Person('Joey', 'Tribbiani');
print('Person p : ${p.firstName} ${p.lastName}');
}

//[super Class]
//person class with basic details
class Person{
  String firstName, lastName;

  //default constructor
  Person(this.firstName, this.lastName);

  //named constructor
  Person.withUpperCase(String firstName,String lastName)
  {
    this.firstName= firstName.toUpperCase();
    this.lastName=lastName.toUpperCase();
  }
}

//[sub Class]
//Employee class shares features of person class
class Employee extends Person{
  int salary;
  Employee(String firstName,String lastName, this.salary): super(firstName,lastName);
}

//student class shares feature of person class
class Student extends Person{
  num score;
  Student(String firstName,String lastName, num marks): super.withUpperCase(firstName,lastName){
    this.score=num.parse(((marks/500)*100).toStringAsFixed(6)); // limit decimal places to 2
  }
}