// main() { 
//    int x = 12; 
//    int y = 0; 
//    int res;  
   
//    try {
//       res = x ~/ y; 
//    } 
//    on IntegerDivisionByZeroException { 
//       print('Cannot divide by zero'); 
//    } 
// } 
// main() { 
//    int x = 12; 
//    int y = 0; 
//    int res;  
   
//    try {  
//       res = x ~/ y; 
//    }  
//    catch(e) { 
//       print(e); 
//    } 
// } 
// main() { 
//    int x = 12; 
//    int y = 0; 
//    int res;  
   
//    try { 
//       res = x ~/ y; 
//    }  
//    on IntegerDivisionByZeroException catch(e) { 
//       print(e); 
//    } 
// } 
// main() { 
//    int x = 12; 
//    int y = 0; 
//    int res;  
   
//    try { 
//       res = x ~/ y; 
//    } 
//    on IntegerDivisionByZeroException { 
//       print('Cannot divide by zero'); 
//    } 
//    finally { 
//       print('Finally block executed'); 
//    } 
// }
// main() { 
//    try { 
//       test_age(-2); 
//    } 
//    catch(e) { 
//       print('Age cannot be negative'); 
//    } 
// }  
// void test_age(int age) { 
//    if(age<0) { 
//       throw new FormatException(); 
//    } 
// }
class AmtException implements Exception { 
   String errMsg() => 'Amount should be greater than zero'; 
}  
void main() { 
   try { 
      withdraw_amt(-1); 
   } 
   catch(e) { 
      print(e.errMsg()); 
   }  
   finally { 
      print('Ending requested operation.....'); 
   } 
}  
void withdraw_amt(int amt) { 
   if (amt <= 0) { 
      throw new AmtException(); 
   } 
}