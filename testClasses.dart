// void main() { 
//    Car c= new Car(); 
//    c.disp(); 
// }  
// class Car {  
//    // field 
//    String engine = "E1001";  
   
//    // function 
//    void disp() { 
//       print(engine); 
//    } 
// }
// void main() { 
//    Car c = new Car('E1001'); 
// } 
// class Car { 
//    Car(String engine) { 
//       print(engine); 
//    } 
// }
// void main() {           
//    Car c1 = new Car.namedConst('E1001');                                       
//    Car c2 = new Car(); 
// }           
// class Car {                   
//    Car() {                           
//       print("Non-parameterized constructor invoked");
//    }                                   
//    Car.namedConst(String engine) { 
//       print("The engine is : ${engine}");    
//    }                               
// }
// void main() { 
//    Car c1 = new Car('E1001'); 
// }  
// class Car { 
//    String engine; 
//    Car(String engine) { 
//       this.engine = engine; 
//       print("The engine is : ${engine}"); 
//    } 
// } 
// class Student { 
//    String name; 
//    int age; 
    
//    String get stud_name { 
//       return name; 
//    } 
    
//    void set stud_name(String name) { 
//       this.name = name; 
//    } 
   
//    void set stud_age(int age) { 
//       if(age<= 0) { 
//         print("Age should be greater than 5"); 
//       }  else { 
//          this.age = age; 
//       } 
//    } 
   
//    int get stud_age { 
//       return age;     
//    } 
// }  
// void main() { 
//    Student s1 = new Student(); 
//    s1.stud_name = 'MARK'; 
//    s1.stud_age = 0; 
//    print(s1.stud_name); 
//    print(s1.stud_age); 
// } 
// void main() { 
//    var obj = new Circle(); 
//    obj.cal_area(); 
// }  
// class Shape { 
//    void cal_area() { 
//       print("calling calc area defined in the Shape class"); 
//    } 
// }  
// class Circle extends Shape {}
// void main() { 
//    var obj = new Leaf(); 
//    obj.str = "hello"; 
//    print(obj.str); 
// }  
// class Root { 
//    String str; 
// }  
// class Child extends Root {}  
// class Leaf extends Child {}  
// //indirectly inherits from Root by virtue of inheritance
// void main() { 
//    Child c = new Child(); 
//    c.m1(12); 
// } 
// class Parent { 
//    void m1(int a){ print("value of a ${a}");} 
// }  
// class Child extends Parent { 
//    @override 
//    void m1(int b) { 
//       print("value of b ${b}"); 
//    } 
// }
// import 'dart:io'; 
// void main() { 
//    Child c = new Child(); 
//    c.m1(12); 
// } 
// class Parent { 
//    void m1(int a){ print("value of a ${a}");} 
// } 
// class Child extends Parent { 
//    @override 
//    void m1(String b) { 
//       print("value of b ${b}");
//    } 
// }
// class StaticMem { 
//    static int num;  
//    static disp() { 
//       print("The value of num is ${StaticMem.num}")  ; 
//    } 
// }  
// void main() { 
//    StaticMem.num = 12;  
//    // initialize the static variable } 
//    StaticMem.disp();   
//    // invoke the static method 
// }
void main() { 
   Child c = new Child(); 
   c.m1(12); 
} 
class Parent { 
   String msg = "message variable from the parent class"; 
   void m1(int a){ print("value of a ${a}");} 
} 
class Child extends Parent { 
   @override 
   void m1(int b) { 
      print("value of b ${b}"); 
      super.m1(13); 
      print("${super.msg}")   ; 
   } 
}