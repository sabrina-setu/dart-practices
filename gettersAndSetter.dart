
void main(){

var ross = Person("Ross Geller Jr.");
print(ross.fullName);

//change name
ross.fullName= "Chandler M. Bing";
print(ross.fullName);
}


class Person{

  //_(underscore) prefix makes variables private to the library
  String _firsName, _lastName;


  //default constructor 
  Person(String name){
    var nameParts= name.split(" ");//split name by space
    this._firsName = nameParts[ 0 ];
    this._lastName=nameParts[ 1 ];
  }
  
  //getter function for 'fullName' property
 String get fullName{
   return"${this._firsName} ${this._lastName}";
 }

  

  //setter function for 'fullName' property
  void set fullName(String name){
    var nameParts =name.split(" ");
    this._firsName=nameParts[ 0 ];
    this._lastName=nameParts[ 1 ];

  }
}