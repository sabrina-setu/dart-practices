

void main(){
//create 'ross' object
Person ross = new Person('Ross','Geller');

print("FullName: ${ross.getFullName()}");
print("Age: ${ross.age}");
}
//Simple Person Class
class Person{

  //instance variables with 'null' value
  String firsName, lastName;

  //instance variables with initial '18' value
  var age ;

  //default constructor function
  Person(this.firsName,this.lastName,[this.age=18]){
    this.firsName=firsName;
    this.lastName=lastName;
    this.age=age;
  }

  //instance methods
  String getFullName (){
    return this.firsName+ " "+this.lastName; // 'this' points to the object itself
  }
}