void main() { 
   String str1 = 'this is a single line string'; 
   String str2 = "this is a single line string"; 
   String str3 = '''this is a multiline line string'''; 
   String str4 = """this is a multiline line string"""; 
   
   print(str1);
   print(str2); 
   print(str3); 
   print(str4); 

   String str5 = "hello"; 
   String str6 = "world"; 
   String res = str5+str6; 
   
   print("The concatenated string : ${res}"); 

      int n=1+1; 
   
   String str7 = "The sum of 1 and 1 is ${n}"; 
   print(str7); 
   
   String str8 = "The sum of 2 and 2 is ${2+2}"; 
   print(str8); 
}